It's very easy to make some words **bold** and other words *italic* with Markdown. 

You can even [link to Google!](http://google.com)


# This is an `<h1>` tag
## This is an `<h2>` tag
###### This is an `<h6>` tag


### Formatting

*This text will be italic*
_This will also be italic_

**This text will be bold**
__This will also be bold__

_You **can** combine them_

~~strikethrough~~


### Lists

* Item 1
* Item 2
  * Item 2a
  * Item 2b

1. Item 1
1. Item 2
1. Item 3
   1. Item 3a
   1. Item 3b

### Links

http://github.com - automatic!

[Google](//google.com)

<myself@example.com>

As Kanye West said:

> We're living the future so
> the present is our past.

I think you should use an
`<addr>` element here instead.


### Tables

| h1    |    h2   |      h3 |
|:------|:-------:|--------:|
| 100   | [a][1]  | ![b][2] |
| *foo* | **bar** | ~~baz~~ |


### Images

![Alt Text Goes Here](/assets/images/Jakesalad.png)
![Here](/assets/images/Jakesalad.png =33%x*)
![Alt](/assets/images/Jakesalad.png =100x140)
![Text](/assets/images/Jakesalad.png =100x*)


### Code Blocks

```
  var code = true;
```