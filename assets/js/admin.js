/*
* Backend javascript main file
*
*/

$(document).ready(function() {

  $('.dataTable').DataTable();

  $('[data-toggle="tooltip"]').tooltip();

  /* Component code editor initialisation */

  if ($('#js-editor').length) {
    var js_editor = ace.edit("js-editor");
        js_editor.setTheme("ace/theme/github");
        js_editor.session.setMode("ace/mode/javascript");

    $(this).on('keyup', function(e) {
      var code = js_editor.getValue();
      $('#js-input').val(code);
    });
  }
    
  if ($('#css-editor').length) {
    var css_editor = ace.edit("css-editor");
        css_editor.setTheme("ace/theme/github");
        css_editor.session.setMode("ace/mode/css");

    $(this).on('keyup', function(e) {
      var code = css_editor.getValue();
      $('#css-input').val(code);
    });
  }

  if ($('#html-editor').length) {
    var html_editor = ace.edit("html-editor");
        html_editor.setTheme("ace/theme/github");
        html_editor.session.setMode("ace/mode/html");

    $(this).on('keyup', function(e) {
      var code = html_editor.getValue();
      $('#html-input').val(code);
    });
  }

  /* Page code editors */

  if ($('#md-editor').length) {

    var md_editor = ace.edit("md-editor");
        md_editor.setTheme("ace/theme/github");
        md_editor.session.setMode("ace/mode/markdown");

    $(this).on('keyup', function(e) {
      var markdown = md_editor.getValue(),
          $preview = $('#md-editor-preview');

      html = previewMarkdown(markdown, $preview);
      $('#md-input').val(markdown);
      $('#html-input').val(html);
    });

    $('.component').on('click', function(e) {
      var usage = "\n";
      usage += $(this).data('usage');
      md_editor.session.insert(md_editor.getCursorPosition(), usage);
    });
  }

  if ($('#md-reference-editor').length) {
    var md_reference = ace.edit("md-reference-editor");
        md_reference.setTheme("ace/theme/github");
        md_reference.session.setMode("ace/mode/markdown");

    $(this).on('keyup', function(e) {
      previewMarkdown(md_reference.getValue(), $('#md-reference-preview'));
    });
  }

  /* Code editor functions */

  function previewMarkdown(markdown, $el) {
    var options = {
      parseImgDimensions: true,
      simplifiedAutoLink: true,
      strikethrough: true,
      tables: true,
      ghCodeBlocks: true,
      smoothLivePreview: true,
      disableForced4SpacesIndentedSublists: true,
      simpleLineBreaks: true,
      encodeEmails: false
      // tasklists: true
    };

    var converter = new showdown.Converter(options),
        html = converter.makeHtml(markdown);
    $el.html(html);
    
    return html; 
  }

  // $('.code').on('keyup', function(e) {
  //   var input = $(this).data('input');
  //   $(input).val($(this).html());
  // });

  $('input[name=name]').on('keyup', function(e) {
    var string = $(this).val(),
        slug = convertToKebab(string),
        snippet = convertToSnake(string);

    $('input[name=slug]').val(slug);
    $('input[name=snippet]').val(snippet);
  });

  // Force the Markdown reference to initialise
  $('.showdown-trigger')
    .children()
    .first()
    .trigger('keyup');
  
  $('.fm-container').richFilemanager({
    // Make sure to set up your config at bower_components/rich-filemanager/config/filemanager.config.js
    "baseUrl": '/bower_components/rich-filemanager'
  });

  $('#file-explorer-open').on('click', function(e) {
    $('#file-explorer').modal('show');
    $(window).trigger('resize'); // Hack because sometimes files wouldnt show up in the preview
  });

});

function convertToKebab(string) {
  return string
    .toLowerCase()
    .replace(/ /g,'-')
    .replace(/[^\w-]+/g,'');
}

function convertToSnake(string) {
  return string
    .toLowerCase()
    .replace(/([a-z])([A-Z])/g, "$1-$2")
    .replace(/\s+/g, '_');
}