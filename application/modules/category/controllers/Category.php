<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth','form_validation','template'));
    $this->load->helper(array('url','language'));
    $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    $this->lang->load('auth');
    $this->load->model('category_model');
  }

  public function index() {
    $this->data['categories'] = $this->category_model->get();

    $this->data['title'] = 'Categories';
    $this->template->load('auth', 'category/index', $this->data);
  }

  public function create() {
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');

    if ($this->form_validation->run() === FALSE) {
      $this->data['title'] = 'New Category';
      $this->template->load('auth', 'category/new', $this->data);
    } else {
      $this->category_model->set();
      redirect('my/categories');
    } 
  }

  public function edit($id) {
    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name', 'required');

    if ($this->form_validation->run() === FALSE) {
      $this->data['category'] = $this->category_model->get_by_id($id);
      $this->data['title'] = 'Edit Category';
      $this->template->load('auth', 'category/edit', $this->data);
    } else {
      $this->category_model->set($id);
      redirect('my/categories');
    } 

    
  }

  public function delete($id) {
    $this->category_model->delete($id);
    redirect('my/categories');
  }

  public function activate($id, $state) {
    $this->category_model->activate($id, $state);
    redirect('my/categories');
  }

}
