<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model {
 
  public function __construct()
  {
    $this->load->database();
  }
  
  public function get($slug = false) {
    if (!$slug) {
      $query = $this->db->get('categories');
      return $query->result();
    }

    $query = $this->db->get_where('categories', array('slug' => $slug));
    return $query->row();
  }
  
  public function get_by_id($id = false) {
      if (!$id) {
        $query = $this->db->get('categories');
        return $query->result();
      }
      $query = $this->db->get_where('categories', array('id' => $id));      
      return $query->row();
  }
  
  public function set($id = false) {
    $this->load->helper('form', 'url');
  
    $slug = url_title($this->input->post('name'), 'dash', true);
    $data = array(
      'name' => $this->input->post('name'),
      'slug' => $slug,
      'description' => $this->input->post('description'),
    );
    
    if (!$id) {
        $result = $this->db->insert('categories', $data);
        $id = $this->db->insert_id();
    } else {
        $this->db->where('id', $id);
        $result = $this->db->update('categories', $data);
    }

    $config = array(
      'upload_path'   => FCPATH.'assets/uploads/'.$this->input->post('image'),
      'allowed_types' => 'gif|jpg|png',
      'max_size'      => '100',
      'max_width'     => '1024',
      'max_height'    => '768',
    );

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('image')) {
      $data = $this->upload->data();
      $this->db->where('id', $id);
      $this->db->update('categories', array('image' => asset_url().'uploads/'.$data['client_name']));
    }
    
    $data['error'] = $this->upload->display_errors();

    return $result;
  }
  
  public function delete($id) {
      $this->db->where('id', $id);
      return $this->db->delete('categories');
  }

  public function activate($id, $state) {
    $this->db->where('id', $id);
    return $this->db->update('categories', array('active' => $state));
  }

}