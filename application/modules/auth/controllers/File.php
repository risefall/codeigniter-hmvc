<?php defined('BASEPATH') OR exit('No direct script access allowed');

class File extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth', 'template'));
    $this->lang->load('auth');
  }

  public function index() {
    $this->data['title'] = 'Files';
    $this->template->load('auth', 'file/index', $this->data);
  }

  public function connect() {

    $app = new \RFM\Application();

    $config = [
      'security' => [
        'readOnly' => false,
        'extensions' => [
          'policy' => 'ALLOW_LIST',
          'restrictions' => [
            'jpg',
            'jpe',
            'jpeg',
            'gif',
            'png',
            'html',
          ],
        ],
      ],
      "upload" => [
        "fileSizeLimit" => 2000000, // 2MB
        "overwrite" => false,
      ],
      "images" => [
        "main" => [
          "autoOrient" => true,
          "maxWidth" => 1280,
          "maxHeight" => 1024,
        ],
        "thumbnail" => [
          "enabled" => true,
          "cache" => true,
          "dir" => "_thumbs/",
          "crop" => true,
          "maxHeight" => 64,
        ]
      ],
      "mkdir_mode" => 0755,
      "logger" => [
        "enabled" => false,
        "file" => null,
      ],
    ];

    // local filesystem storage
    $local = new \RFM\Repository\Local\Storage($config);
    $app->setStorage($local);
    $local->setRoot('/assets/uploads', true, true);

    // local filesystem API
    $app->api = new RFM\Api\LocalApi();
    $app->run();

  }

}
