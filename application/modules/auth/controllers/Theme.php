<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Symfony\Component\Yaml\Yaml;

class Theme extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth', 'template'));
    $this->lang->load('auth');
  }

  public function index() {
    $default_file = FCPATH.'assets/yaml/theme.default.yaml';
    $saved_file = FCPATH.'assets/yaml/theme.yaml';
    
    // Parse YAML into PHP arrays
    $default_yaml = Yaml::parseFile($default_file);
    $saved_yaml = Yaml::parseFile($saved_file); // TODO - load file from DB instead - reason: versioning

    if ($saved_yaml) {
      // Set the saved values in the array from the default file
      foreach($saved_yaml as $section_key => $section) {
        foreach($section as $setting_key => $setting) {
          $default_yaml['theme'][$section_key][$setting_key]['value'] = $setting;
        }
      }
    }
    $this->data['theme_config'] = $default_yaml['theme'];
    $this->data['title'] = 'Theme';
    $this->template->load('auth', 'theme/index', $this->data);
  }

  public function update() {
    // Write $_POST array into YAML file
    $yaml = Yaml::dump($_POST);
    file_put_contents(FCPATH.'assets/yaml/theme.yaml', $yaml); // TODO - save file from DB instead - reason: versioning

    $this->build($_POST);

    redirect('my/theme');
  }

  public function build($config) {
    // augment array as SCSS variables

    // build SCSS file

    if ($config) {
      $scss = "/* This file has been compiled by application/modules/auth/controllers/Theme.php - do not edit */\n\n";
      foreach ($config as $section_key => $section) {
        foreach ($section as $setting_key => $setting) {
          $scss .= '$'.$section_key.'-'.$setting_key.': '.$setting.";\n";
        }
      }

      file_put_contents(FCPATH.'assets/scss/theme/_settings.scss', $scss);

    }

  }

}
