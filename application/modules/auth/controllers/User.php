<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth','form_validation','template'));
    $this->load->helper(array('url','language'));
    $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    $this->lang->load('auth');
  }

  // redirect if needed, otherwise display the user list
  public function index()
  {

    if (!$this->ion_auth->logged_in())
    {
      // redirect them to the login page
      redirect(base_url('login'), 'refresh');
    }
    elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {
      // redirect them to the home page because they must be an administrator to view this
      return show_error('You must be an administrator to view this page.');
    }
    else
    {
      // set the flash data error message if there is one
      $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

      //list the users
      $this->data['users'] = $this->ion_auth->users()->result();
      foreach ($this->data['users'] as $k => $user)
      {
        $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
      }

      $this->data['title'] = 'Users';
      $this->template->load('auth', 'user/index', $this->data);
    }
  }

  public function activate($id, $state) {
    if ($state) {
      $this->ion_auth->activate($id);
    } else {
      $this->ion_auth->deactivate($id);
    }

    redirect('my/users');
  }

}