<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth', 'template'));
    $this->lang->load('auth');
    $this->load->model('auth/settings_model');
  }

  public function index() {
    $this->load->helper('form');

    $this->data['settings'] = $this->settings_model->get();

    if ($this->input->post()) {
      $this->settings_model->set();  
    }

    $this->data['title'] = 'Settings';
    $this->template->load('auth', 'settings/index', $this->data);
  }

  public function save() {
    // redirect('my/settings', 'refresh');
  }

  public function get($key = false) {
    // get all settings

    // return $settings
  }

  public function set($input) {
    // post array = key + value

    // foreach >> save()

    // return $result
  }

}
