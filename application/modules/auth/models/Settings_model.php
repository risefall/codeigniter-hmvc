<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model {
 
  public function __construct()
  {
    $this->load->database();
  }
  
  public function get() {
    $this->db->from('settings');
    return $this->db->get()->result();
  }
  
  public function get_by_key($key = false) {
      if (!$key) {
        $query = $this->db->get('settings');
        return $query->result();
      }
      $query = $this->db->get_where('settings', array('key' => $key));      
      return $query->row();
  }

  public function get_array()
  {
    $query = $this->db->get('settings');
    if ($query->num_rows === 0) {
      return false;
    }

    $settings = $query->result();
    $return = array();
    foreach($settings as $setting) {
      $return[$setting->key] = $setting->value;
    }

    return $return;
  }
  
  public function set($id = false) {
    $data = $this->input->post();

    foreach ($data as $key => $value) {
      $setting = $this->db->get_where('settings', array('key' =>$key))->row();

      if ($setting) {
        // Setting exists - update
        $query = $this->db
          ->where('id', $setting->id)
          ->update('settings', array('value' => $value));
      } else {
        // Setting does not exist - create
        $query = $this->db->insert('settings', array('key' => $key, 'value' => $value));
      }
    }

    return $query;
  }
  
  public function delete($id) {
      $this->db->where('id', $id);
      return $this->db->delete('pages');
  }

  public function activate($id, $state) {
    $this->db->where('id', $id);
    return $this->db->update('pages', array('active' => $state));
  }

}