<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Component_interface_model extends CI_Model {
 
  public function __construct() {
    $this->load->database();
  }
  
  public function get_categories($request) {
    if (isset($request[2])) {
      $query = $this->db->get_where('categories', array('slug' => $request[2]));
      return $query->row();
    }

    $query = $this->db->get_where('categories', array('active' => 1));
    return $query->result();
  }

  public function get_category_with_items($request) {
    if (isset($request[2])) {
      $category = $this->db
        ->get_where('categories', array('slug' => $request[2]))->row();

      $this->db
        ->select('items.*')
        ->from('items')
        ->join('items_categories', 'items.id = items_categories.item_id')
        ->join('categories', 'categories.id = items_categories.category_id')
        ->where('categories.id', $category->id);

      $category->items = $this->db->get()->result();

      return $category;  
    } else {
      return false;
    }
    
  }

}