<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Component_model extends CI_Model {
 
  public function __construct()
  {
    $this->load->database();
  }
  
  public function get($slug = false) {
    if (!$slug) {
      $query = $this->db->get('components');
      return $query->result();
    }

    $query = $this->db->get_where('components', array('slug' => $slug));
    return $query->row();
  }
  
  public function get_by_id($id = false) {
      if (!$id) {
        $query = $this->db->get('components');
        return $query->result();
      }
      $query = $this->db->get_where('components', array('id' => $id));      
      return $query->row();
  }

  /*
  * @param $query Array
  */

  public function get_where($queries) {
    if (!$queries) {
      $query = $this->db->get('components');
      return $query->result();
    }

    $this->db->from('components');
    foreach ($queries as $query) {
      $this->db->where($query);
    }
    return $this->db->get()->result();
  }
  
  public function set($id = false) {
    $this->load->helper('url');
    $slug = url_title($this->input->post('name'), 'dash', true);
    $data = array(
      'name' => $this->input->post('name'),
      'slug' => $slug,
      'description' => $this->input->post('description'),
      'usage_example' => $this->input->post('usage_example'),
      'html' => $this->input->post('html'),
      'css' => $this->input->post('css'),
      'js' => $this->input->post('js'),
    );
    
    if (!$id) {
        $result = $this->db->insert('components', $data);
    } else {
        $this->db->where('id', $id);
        $result = $this->db->update('components', $data);
    }
    return $result;
  }
  
  public function delete($id) {
      $this->db->where('id', $id);
      return $this->db->delete('components');
  }

  public function activate($id, $state) {
    $this->db->where('id', $id);
    return $this->db->update('components', array('active' => $state));
  }

}