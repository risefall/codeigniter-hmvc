<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Component extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth','form_validation','template'));
    $this->load->helper(array('url','language'));
    $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    $this->lang->load('auth');
    $this->load->model('component_model');
    $this->load->model('component_interface_model');
  }

  public function index() {
    $this->data['components'] = $this->component_model->get();;

    $this->data['title'] = 'Components';
    $this->template->load('auth', 'component/index', $this->data);
  }

  public function create() {
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');

    $endpoints = get_class_methods($this->component_interface_model);
    $exclude = array(
      '__get',
      '__construct'
    );
    $this->data['endpoints'] = array_diff($endpoints, $exclude);

    if ($this->form_validation->run() === false) {
      $this->data['guide'] = file_get_contents(FCPATH.'assets/text/markdown_guide.md'); 
      $this->data['title'] = 'New Component';
      $this->template->load('auth', 'component/new', $this->data); 
    } else {
      $this->component_model->set();
      redirect('my/components');
    } 
  }

  public function edit($id) {
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');

    $endpoints = get_class_methods($this->component_interface_model);
    $exclude = array(
      '__get',
      '__construct'
    );
    $this->data['endpoints'] = array_diff($endpoints, $exclude);

    if ($this->form_validation->run() === false) {
      $this->data['component'] = $this->component_model->get_by_id($id);
      $this->data['guide'] = file_get_contents(FCPATH.'assets/text/markdown_guide.md'); 
      $this->data['title'] = 'Edit component';
      $this->template->load('auth', 'component/edit', $this->data);
    } else {
      $this->component_model->set($id);
      redirect('my/components');
    } 
    
  }

  public function delete($id) {
    $this->component_model->delete($id);
    redirect('my/components');
  }

  public function activate($id, $state) {
    $this->component_model->activate($id, $state);
    redirect('my/components');
  }

}
