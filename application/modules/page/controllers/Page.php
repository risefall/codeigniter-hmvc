<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Symfony\Parser;

class Page extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth','form_validation','template'));
    $this->load->helper(array('url','language'));
    $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    $this->lang->load('auth');
    $this->load->model('page_model');
  }

  public function index() {
    $this->data['pages'] = $this->page_model->get();;

    $this->data['title'] = 'Pages';
    $this->template->load('auth', 'page/index', $this->data);
  }

  public function edit($id = false) {
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');

    $this->load->model('component/component_model');
    $this->data['components'] = $this->component_model->get();

    if ($this->form_validation->run() === FALSE) {
      $this->data['guide'] = file_get_contents(FCPATH.'assets/text/markdown_guide.md'); 
      if ($id) {
        $this->data['page'] = $this->page_model->get_by_id($id);
      }
      $this->data['title'] = 'New Page';
      $this->template->load('auth', 'page/edit', $this->data); 
    } else {
      $this->page_model->set($id);
      redirect('my/pages');
    } 
  }

  public function delete($id) {
    $this->page_model->delete($id);
    redirect('my/pages');
  }

  public function activate($id, $state) {
    $this->page_model->activate($id, $state);
    redirect('my/pages');
  }

  public function update($key, $value) {
    $this->page_model->update($key, $value);
  }

  public function view($slug = "home") {
    $this->load->model('component/component_model');

    if ($slug === 'home') {
      $page = $this->page_model->get_home_page();
    } else {
      $page = $this->page_model->get($slug);  
    }
    

    $this->load->model('page/page_model');
    $this->data['pages'] = $this->page_model->get();

    $rendered = $this->_render_components($page->html);

    $loader = new Twig_Loader_Array(array(
        'page' => $rendered['html'],
    ));

    $this->data['styles'] = "";
    $this->data['scripts'] = "";
    $this->data['component_data'] = false;

    $this->load->model('component/component_interface_model');
    $uri = $this->uri->segments;

    foreach ($rendered['components'] as $component) {
      $cmp = $this->component_model->get($component);

      $this->data['styles'] .= $this->_compile_style($cmp);
      $this->data['scripts'] .= $this->_compile_script($cmp);

      if ($cmp->endpoint && method_exists($this->component_interface_model, $cmp->endpoint)) {
        $this->data['component_data'] = $this->component_interface_model->{$cmp->endpoint}($uri);
      }
    }

    $twig = new Twig_Environment($loader, array(
        'debug' => true,
        // 'cache' => APPPATH.'cache',
    ));
    $twig->addExtension(new Twig_Extension_StringLoader());
    $twig->addExtension(new Twig_Extension_Debug());

    $this->data['user'] = $this->ion_auth->user()->row();
    $this->data['title'] = $page->name;
    $this->data['page'] = $twig->render('page', $this->data);

    $this->template->load('page', 'view', $this->data);
  }

  /*
   * Given a single component, compile its CSS and render as valid HTML
   */

  private function _compile_style($component) {
    $CSS = "<style>";
    $CSS .= $component->css;
    $CSS .= "</style>";

    return $CSS;
  }

  /*
   * Given a single component, compile its JS and render as valid HTML
   */

  private function _compile_script($component) {
    $JS = "<script>";
    $JS .= $component->js;
    $JS .= "</script>";

    return $JS;
  }

  /*
   * Given an array of components, compile their CSS and render as valid HTML
   */

  private function _compile_component_styles($components) {
    $CSS = "<style>";
    foreach ($components as $component) {
      $CSS .= $component->css;
    }
    $CSS .= "</style>";

    return $CSS;
    
  }

  /*
   * Given an array of components, compile their JS and render as valid HTML
   */

  private function _compile_component_scripts($components) {
    $JS = "<script>";
    foreach ($components as $component) {
      $JS .= $component->js;
    }
    $JS .= "</script>";

    return $JS;
  }

  /*
   * Find component codes in page html and render them
   */

  private function _render_components($html) {

    $rendered_component = '';
    $component_array = array();
    
    // Find the component codes
    preg_match_all('/{#(.*?)#}/', $html, $matches, PREG_SET_ORDER);

    foreach ($matches as $val) {

      $component_code = $val[1];
      // Remove the (params) from the code so we can use it to get the component
      $component_code = trim(substr($component_code, 0, strpos($component_code, "(")));

      array_push($component_array, $component_code);

      // Find the params
      preg_match_all('/(\(.*?\))/', $val[1], $cmatches, PREG_SET_ORDER);

      foreach($cmatches as $param_string) {
        $params = str_replace(array('(', ')', '\''), '', $param_string[1]);
        $params = explode(',', $params);
        // Get the component html from the code
        $component = $this->component_model->get($component_code);
        
        if (isset($component) && $component->html) {
          $rendered_component = htmlspecialchars($component->html);

          // Replace the params $1 $2 $3 etc
          foreach ($params as $i => $param) {
            $i++;

            $rendered_component = str_replace("{".$i."}", $param, $rendered_component);
          }
        }
      }

      // Replace the component code with the component html
      $html = str_replace($val[0], $rendered_component, $html);
    }

    $return = array(
      'html' => htmlspecialchars_decode($html),
      'components' => $component_array,
    );

    return $return;
  }

}
