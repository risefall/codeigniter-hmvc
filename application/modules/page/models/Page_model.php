<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends CI_Model {
 
  public function __construct()
  {
    $this->load->database();
  }
  
  public function get($slug = false) {
    if (!$slug) {
      $this->db->order_by('order', 'ASC');
      $query = $this->db->get('pages');
      return $query->result();
    }

    $query = $this->db->get_where('pages', array('slug' => $slug));
    return $query->row();
  }
  
  public function get_by_id($id = false) {
      if (!$id) {
        $query = $this->db->get('pages');
        return $query->result();
      }
      $query = $this->db->get_where('pages', array('id' => $id));      
      return $query->row();
  }

  public function get_home_page() {
      $query = $this->db->get_where('pages', array('home_page' => 1));      
      return $query->row();
  }
  
  public function set($id = false) {
    $this->load->helper('url');
    $slug = url_title($this->input->post('name'), 'dash', true);
    $data = array(
      'name' => $this->input->post('name'),
      'slug' => $slug,
      'markdown' => $this->input->post('markdown'),
      'html' => $this->input->post('html'),
    );
    
    if (!$id) {
        $result = $this->db->insert('pages', $data);
    } else {
        $this->db->where('id', $id);
        $result = $this->db->update('pages', $data);
    }
    return $result;
  }
  
  public function delete($id) {
      $this->db->where('id', $id);
      return $this->db->delete('pages');
  }

  public function activate($id, $state) {
    $this->db->where('id', $id);
    return $this->db->update('pages', array('active' => $state));
  }

}