<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_model extends CI_Model {
 
  public function __construct() {
    $this->load->database();
  }
  
  public function get($slug = false) {
    if (!$slug) {
      $items = $this->db->get('items')->result();
    } else {
      $items = $this->db->get_where('items', array('slug' => $slug))->row();
    }

    foreach ($items as $item) {
      $this->db
        ->select('categories.*')
        ->from('items')
        ->join('items_categories', 'items.id = items_categories.item_id')
        ->join('categories', 'categories.id = items_categories.category_id')
        ->where('items.id', $item->id);

      $item->categories = $this->db->get()->result();
    }
    return $items;
  }
  
  public function get_by_id($id = false) {
      if (!$id) {
        $query = $this->db->get('items');
        $item = $query->result();
      } else {
        $query = $this->db->get_where('items', array('id' => $id));      
        $item = $query->row();  
      }

      $this->db
        ->select('categories.*')
        ->from('items')
        ->join('items_categories', 'items.id = items_categories.item_id')
        ->join('categories', 'categories.id = items_categories.category_id')
        ->where('items.id', $item->id);
      
      $item->categories = $this->db->get()->result();

      return $item;

  }
  
  public function set($id = false) {
    $this->load->helper('url');

    $this->db->trans_start();
  
    $slug = url_title($this->input->post('name'), 'dash', true);
    $data = array(
      'name' => $this->input->post('name'),
      'slug' => $slug,
      'description' => $this->input->post('description'),
      'phone' => $this->input->post('phone'),
      'email' => $this->input->post('email'),
      'website' => $this->input->post('website'),
      'address' => $this->input->post('address'),
    );
    
    if (!$id) {
        $result = $this->db->insert('items', $data);
        $id = $this->db->insert_id();
    } else {
        $this->db->where('id', $id);
        $result = $this->db->update('items', $data);
    }

    // remove all items_categories for this id
    $this->db->where('item_id', $id);
    $this->db->delete('items_categories');

    // insert all many_to_many entries for items_categories
    $categories = $this->input->post('categories');

    if ($categories) {
      foreach($categories as $category) {
        $this->db->insert(
          'items_categories', 
          array(
            'item_id' => $id, 
            'category_id' => $category
          )
        );
      }
    }

    // upload image
    $config = array(
      'upload_path'   => FCPATH.'assets/uploads/'.$this->input->post('image'),
      'allowed_types' => 'gif|jpg|png',
      'max_size'      => '100',
      'max_width'     => '1024',
      'max_height'    => '768',
    );

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('image')) {
      $data = $this->upload->data();
      $this->db->where('id', $id);
      $this->db->update('items', array('image' => asset_url().'uploads/'.$data['client_name']));
    }
    
    $data['error'] = $this->upload->display_errors();

    $this->db->trans_complete();
    return $result;
  }
  
  public function delete($id) {
      $this->db->where('id', $id);
      return $this->db->delete('items');
  }

  public function activate($id, $state) {
    $this->db->where('id', $id);
    return $this->db->update('items', array('active' => $state));
  }

}