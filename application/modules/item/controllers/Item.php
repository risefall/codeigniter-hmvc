<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends MX_Controller {
    
  private $data; // fix for HMVC

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth','form_validation','template'));
    $this->load->helper(array('url','language'));
    $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    $this->lang->load('auth');
    $this->load->model('item_model');
    $this->load->model('category/category_model');
  }

  public function index() {
    $this->data['items'] = $this->item_model->get();

    $this->data['title'] = 'Items';
    $this->template->load('auth', 'item/index', $this->data);
  }

  public function create() {
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');

    if ($this->form_validation->run() === FALSE) {

      $this->data['categories'] = $this->category_model->get();

      $this->data['title'] = 'New Item';
      $this->template->load('auth', 'item/new', $this->data);
    } else {
      $this->item_model->set();
      redirect('my/items');
    } 
  }

  public function edit($id) {
    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Name', 'required');

    if ($this->form_validation->run() === FALSE) {

      $this->data['categories'] = $this->category_model->get();
      
      $this->data['item'] = $this->item_model->get_by_id($id);
      $this->data['title'] = 'Edit item';
      $this->template->load('auth', 'item/edit', $this->data);
    } else {
      $this->item_model->set($id);
      redirect('my/items');
    } 

    
  }

  public function delete($id) {
    $this->item_model->delete($id);
    redirect('my/items');
  }

  public function activate($id, $state) {
    $this->item_model->activate($id, $state);
    redirect('my/items');
  }

}
