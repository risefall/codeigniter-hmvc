<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['default_controller']                  = 'page/view';

$route['my/user/activate/(:num)/(:num)']      = 'auth/user/activate/$1/$2';
$route['my/user/edit/(:num)']                 = 'auth/edit_user/$1';
$route['my/user/new']                         = 'auth/create_user';
$route['my/user/new_group']                   = 'auth/create_group';
$route['my/users']                            = 'auth/user/index';
$route['my/settings/save']                    = 'auth/settings/save';
$route['my/settings']                         = 'auth/settings/index';
$route['my/theme/update']                     = 'auth/theme/update';
$route['my/theme/generate']                   = 'auth/theme/generate';
$route['my/theme']                            = 'auth/theme/index';
$route['my/file/activate/(:num)/(:num)']      = 'auth/file/activate/$1/$2';
$route['my/file/edit/(:num)']                 = 'auth/file/edit/$1';
$route['my/file/delete/(:num)']               = 'auth/file/delete/$1';
$route['my/file/new']                         = 'auth/file/create';
$route['my/files']                            = 'auth/file/index';
$route['filemanager/connect']                 = 'auth/file/connect';
$route['my/page/update/(:num)/(:any)/(:any)'] = 'page/update/$1/$2/$3';
$route['my/page/activate/(:num)/(:num)']      = 'page/activate/$1/$2';
$route['my/page/edit/(:num)']                 = 'page/edit/$1';
$route['my/page/edit']                        = 'page/edit';
$route['my/page/delete/(:num)']               = 'page/delete/$1';
$route['my/pages']                            = 'page/index';
$route['my/component/activate/(:num)/(:num)'] = 'component/activate/$1/$2';
$route['my/component/edit/(:num)']            = 'component/edit/$1';
$route['my/component/delete/(:num)']          = 'component/delete/$1';
$route['my/component/new']                    = 'component/create';
$route['my/components']                       = 'component/index';
$route['my/categories']                       = 'category/index';
$route['my/category/activate/(:num)/(:num)']  = 'category/activate/$1/$2';
$route['my/category/edit/(:num)']             = 'category/edit/$1';
$route['my/category/delete/(:num)']           = 'category/delete/$1';
$route['my/category/new']                     = 'category/create';
$route['my/categories']                       = 'category/index';
$route['my/item/activate/(:num)/(:num)']      = 'item/activate/$1/$2';
$route['my/item/edit/(:num)']                 = 'item/edit/$1';
$route['my/item/delete/(:num)']               = 'item/delete/$1';
$route['my/item/new']                         = 'item/create';
$route['my/items']                            = 'item/index';
$route['my']                                  = "auth/index";


$route['login']                               = 'auth/login';
$route['logout']                              = 'auth/logout';

$route['(.+)']                                = 'page/view/$1';