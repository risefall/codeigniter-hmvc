# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.70.45 (MySQL 5.6.33)
# Database: pcs
# Generation Time: 2018-02-16 06:07:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `image`, `active`, `created_at`, `updated_at`)
VALUES
	(2,'Example Category','example-category','An example category','http://codeigniter.loc:8888/assets/uploads/Jakesalad.png',1,'2018-02-02 17:17:31','2018-02-13 14:39:19');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table components
# ------------------------------------------------------------

DROP TABLE IF EXISTS `components`;

CREATE TABLE `components` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `usage_example` varchar(255) DEFAULT NULL,
  `html` text,
  `css` text,
  `js` text,
  `endpoint` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `components` WRITE;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;

INSERT INTO `components` (`id`, `name`, `slug`, `description`, `usage_example`, `html`, `css`, `js`, `endpoint`, `active`, `created_at`, `updated_at`)
VALUES
	(2,'Download File','download-file','A block with a title and a button to download a file','{# download-file(\'title\', \'link_text\', \'link_url\') #}','<div class=\"box\">\r\n    <span class=\"h6\">{1}</span>\r\n    <a class=\"btn btn-primary float-right\" href=\"{3}\" download>{2}</a>\r\n</div>','.box {\r\n    padding: 28px 10px;\r\n    background: #EEE;\r\n    border: 1px solid #DCDCDC;\r\n    clear: both;\r\n}','',NULL,1,'2018-02-08 16:50:37','2018-02-16 14:41:25'),
	(6,'Search','search','Ajax search input','{{ search(\'action\', \'method\') }}','<form class=\"cs-form\" action=\"{{ $1 }}\" method=\"{{ $2 }}\">\r\n    <input class=\"cs-input\" type=\"text\" name=\"query\" />\r\n    <div class=\"cs-results\"></div>\r\n</form>','.cs-input {\r\n    background: black;\r\n    color: grey;\r\n    border: none;\r\n}','$(\'.cs-input\').on(\'keyup\', function(e) {\r\n    var action = $(this).parent().attr(\'action\'),\r\n        method = $(this).parent().attr(\'method\'),\r\n        query = $(this).val();\r\n    \r\n    $.ajax(\r\n        action, \r\n        method,\r\n        query,\r\n        function(response) {\r\n            if(response) {\r\n                // Append response.data to results div\r\n            } else {\r\n                // Append \'no results\' message to results div\r\n            }\r\n        });\r\n});',NULL,1,'2018-02-14 12:30:41','2018-02-16 09:41:00'),
	(7,'Blog Feed','blog-feed','A simple blog feed created from a JSON endpoint','{# blog-feed(`url`) #}','<div class=\"blog-feed\" data-url=\"{1}\"></div>','','if ($(\'.blog-feed\').length) {\r\n    var $elem = $(\'.blog-feed\');\r\n\r\n    $.each($elem, function(i, el) {\r\n        \r\n        var url = $(el).data(\'url\');\r\n        $.ajax(url)\r\n            .success(function(response) {\r\n                var json = JSON.parse(response);\r\n                    $.each(json, function(j, post) {\r\n                        var html = \"<div class=\'post\'><p>\"+ post.name +\"</p><p>\"+ post.date_created +\"</p><p>\"+ post.excerpt +\"</p><a href=\'\" + post.url + \"\'>Read More</a></div>\";\r\n                        $(el).append(html);\r\n                    });\r\n            })\r\n            .error(function() {\r\n                $(el).append(\"<p>No posts found</p>\");\r\n            });\r\n    });\r\n}',NULL,1,'2018-02-14 14:20:06','2018-02-16 12:44:55'),
	(8,'Contact Form','contact-form','A simple contact form','{# contact-form(\'referenceId\') #}','<div class=\"col-md-6\">\r\n    <form action=\"/send/message\" method=\"POST\">\r\n        <div class=\"form-group\">\r\n            <label>Name</label> <br />\r\n            <input type=\"text\" name=\"name\" class=\"form-control\" value=\"{{ user.first_name }}\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label>Email</label> <br />\r\n            <input type=\"text\" name=\"name\" class=\"form-control\" value=\"{{ user.email }}\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label>Message</label> <br />\r\n            <textarea class=\"form-control\" name=\"message\" rows=\"5\"></textarea>\r\n        </div>\r\n        <button type=\"submit\" class=\"btn btn-lg btn-info\">Send</button>\r\n    </form>\r\n</div>','','',NULL,1,'2018-02-14 15:13:32','2018-02-16 16:03:59'),
	(10,'Categories List','categories-list','A list of all categories','{# categories-list() #}','{% set categories = component_data %}\r\n\r\n{% for category in categories %}\r\n\r\n    <div class=\"category card col-md-4\" data-compare=\"{{ category.name }}\">\r\n      <div class=\"card-bg-img\" style=\"background-image:url(\'{{ category.image }}\');\"></div>\r\n      <!--<img class=\"card-img-top\" src=\"{{ category.image }}\" alt=\"{{ category.name }}\">-->\r\n      <div class=\"card-body\">\r\n        <h5 class=\"card-title\">{{ category.name }}</h5>\r\n        <p class=\"card-text\">{{ category.description }}</p>\r\n        <a href=\"/service/{{ category.slug }}\" class=\"btn btn-primary\">View Services</a>\r\n      </div>\r\n    </div>\r\n\r\n{% endfor %}','.category .card-bg-img {\r\n    height: 200px;\r\n    width: 100%;\r\n    background-size: cover;\r\n}','','get_categories',1,'2018-02-15 14:45:23','2018-02-16 12:23:46'),
	(11,'Category Items','category-items','A list of items within a category','{# category-items() #}','{% set category = component_data %}\r\n\r\n{% for item in category.items %}\r\n\r\n<section class=\"item\" data-compare=\"{{ item.name }}\">\r\n<div class=\"container py-3\">\r\n    <div class=\"card\">\r\n        <div class=\"row \">\r\n            <div class=\"col-md-2\">\r\n                <img src=\"{{ item.image }}\" class=\"category-item-image w-100\">\r\n            </div>\r\n            <div class=\"col-md-10 px-3\">\r\n                <div class=\"card-block px-3 m-4\">\r\n                    <h4 class=\"card-title\">{{ item.name }}</h4>\r\n                    <p class=\"card-text\">{{ item.description }}</p>\r\n                    <hr>\r\n                    <a class=\"mr-4\" href=\"facetime:{{ item.phone }}\"><i class=\"fa fa-phone\"></i> {{ item.phone }}</a>\r\n                    <a class=\"mr-4\" href=\"mailto:{{ item.email }}\"><i class=\"fa fa-at\"></i> {{ item.email }}</a>\r\n                    <a class=\"mr-4\" href=\"http://maps.google.com/maps?q={{ item.address }}\"><i class=\"fa fa-map-marker\"></i> {{ item.address }}</a>\r\n                    <a class=\"mr-4\" href=\"{{ item.website }}\"><i class=\"fa fa-globe\"></i> {{ item.website }}</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n</section>\r\n\r\n{% endfor %}','','','get_category_with_items',1,'2018-02-15 16:15:04','2018-02-16 15:13:56'),
	(12,'Services Intro','services-intro','Lead-in content/header for the Services page','{# services-intro #}','<div class=\"row\">\r\n    <div class=\"col-md-8\">\r\n        <h1>Welcome, {{ user.first_name|title }}</h1>\r\n        <p>\r\n            The Private Concierge Service for Grace Tarneit is designed to give you a guide to the local area, as well as make moving into your new home a breeze.\r\n            <a href=\"/terms-and-conditions\">View our Terms &amp; Conditions</a>\r\n        </p>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n        <label for=\"intro-search\" class=\"mt-4\">Search Services</label> <br />\r\n        <div class=\"input-group\">\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"e.g. Electricity\" name=\"intro-search\" id=\"intro-search\">\r\n          <div class=\"input-group-append\">\r\n            <button class=\"btn btn-secondary\" type=\"button\"><i class=\"fas fa-search\"></i></button>\r\n          </div>\r\n        </div>\r\n    </div>\r\n</div>','','$(\'#intro-search\').on(\'keyup\', function(e) {\r\n    query = $(this).val().toLowerCase();\r\n    \r\n    if ($(\'.category.card\').length) {\r\n        $(\'.category.card\').each(function(i, el) {\r\n            compare = $(el).data(\'compare\').toLowerCase();\r\n            if (compare.indexOf(query) == \'-1\') {\r\n                $(el).addClass(\'hide\');\r\n            } else {\r\n                $(el).removeClass(\'hide\');\r\n            }\r\n        });\r\n    }\r\n});',NULL,1,'2018-02-16 09:39:44','2018-02-16 12:26:20'),
	(13,'Service Details Intro','service-details-intro','Intro and search bar for the Service Details page','{# service-details-intro() #}','{% set category = component_data %}\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-md-8\">\r\n        <h2>{{ category.name|title }}</h2>\r\n        <p>{{ category.description|capitalize }}</p>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n        <label for=\"intro-search\" class=\"mt-4\">Search Services</label> <br />\r\n        <div class=\"input-group\">\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"e.g. Electricity\" name=\"intro-search\" id=\"intro-search\">\r\n          <div class=\"input-group-append\">\r\n            <button class=\"btn btn-secondary\" type=\"button\"><i class=\"fas fa-search\"></i></button>\r\n          </div>\r\n        </div>\r\n    </div>\r\n</div>','','$(\'#intro-search\').on(\'keyup\', function(e) {\r\n    query = $(this).val().toLowerCase();\r\n    \r\n    if ($(\'section.item\').length) {\r\n        $(\'section.item\').each(function(i, el) {\r\n            compare = $(el).data(\'compare\').toLowerCase();\r\n            if (compare.indexOf(query) == \'-1\') {\r\n                $(el).addClass(\'hide\');\r\n            } else {\r\n                $(el).removeClass(\'hide\');\r\n            }\r\n        });\r\n    }\r\n});','get_categories',1,'2018-02-16 10:20:39','2018-02-16 12:31:14');

/*!40000 ALTER TABLE `components` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;

INSERT INTO `items` (`id`, `name`, `slug`, `description`, `image`, `phone`, `email`, `address`, `website`, `active`, `created_at`, `updated_at`)
VALUES
	(3,'Example Item','example-item','An example item','http://codeigniter.loc:8888/assets/uploads/Jakesalad.png','0411 222 333','example@email.com','123 Example St, Exampletown','www.example.com',1,'2018-02-07 15:48:15','2018-02-13 14:40:02');

/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table items_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `items_categories`;

CREATE TABLE `items_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `items_categories` WRITE;
/*!40000 ALTER TABLE `items_categories` DISABLE KEYS */;

INSERT INTO `items_categories` (`id`, `item_id`, `category_id`)
VALUES
	(38,1,2),
	(39,1,7),
	(40,1,8),
	(49,3,2);

/*!40000 ALTER TABLE `items_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `markdown` text,
  `html` text,
  `active` tinyint(1) DEFAULT '0',
  `home_page` tinyint(1) DEFAULT '0',
  `main_navigation` tinyint(1) DEFAULT '1',
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `name`, `slug`, `markdown`, `html`, `active`, `home_page`, `main_navigation`, `order`, `created_at`, `updated_at`)
VALUES
	(1,NULL,'Documentation','documentation','#### Documentation Download\r\n\r\n###### Builders documentation\r\n\r\nNeed the documentation for one of our builders? You can download it below:\r\n\r\n{# download-file(\'Memorandum of Common Provisions\', \'Download\', \'/\') #}\r\n\r\n{# download-file(\'Plan of subdivision - Stage 1\', \'Download\', \'/\') #}\r\n\r\n{# download-file(\'Engineering - Stage 1\', \'Download\', \'/\') #}\r\n\r\n###### Design Guidelines\r\n\r\nBuilding within Grace Tarneit? Download our design guidelines to ensure your home meets the required regulations\r\n\r\n###### Why Design Guidelines?\r\n\r\nThe Design Guidelines document has been prepared to assist Grace Tarneit owners, designers and builders through the planning and construction process.  Design guidelines are a means of ensuring the Grace Tarneit vision is upheld. The document ensures appropriate amenity is delivered to keep your investment protected.\r\n\r\n{# download-file(\'Grace Tarneit Design Guidelines\', \'Download\', \'/\') #}\r\n\r\n','<h4 id=\"documentationdownload\">Documentation Download</h4>\r\n<h6 id=\"buildersdocumentation\">Builders documentation</h6>\r\n<p>Need the documentation for one of our builders? You can download it below:</p>\r\n<p>{# download-file(\'Memorandum of Common Provisions\', \'Download\', \'/\') #}</p>\r\n<p>{# download-file(\'Plan of subdivision - Stage 1\', \'Download\', \'/\') #}</p>\r\n<p>{# download-file(\'Engineering - Stage 1\', \'Download\', \'/\') #}</p>\r\n<h6 id=\"designguidelines\">Design Guidelines</h6>\r\n<p>Building within Grace Tarneit? Download our design guidelines to ensure your home meets the required regulations</p>\r\n<h6 id=\"whydesignguidelines\">Why Design Guidelines?</h6>\r\n<p>The Design Guidelines document has been prepared to assist Grace Tarneit owners, designers and builders through the planning and construction process.  Design guidelines are a means of ensuring the Grace Tarneit vision is upheld. The document ensures appropriate amenity is delivered to keep your investment protected.</p>\r\n<p>{# download-file(\'Grace Tarneit Design Guidelines\', \'Download\', \'/\') #}</p>',1,0,1,4,'2018-02-07 12:09:59','2018-02-16 14:42:05'),
	(12,NULL,'About','about','<div class=\"text-center\">\r\n\r\n# About\r\n\r\nIntro copy about Private Concierge Service goes here. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non.\r\n\r\nVivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta felis euismod semper. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla.\r\n\r\n<a class=\"btn btn-primary mt-4\" href=\"/services\">View Services</a>','<p><div class=\"text-center\"></p>\r\n<h1 id=\"about\">About</h1>\r\n<p>Intro copy about Private Concierge Service goes here. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non.</p>\r\n<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta felis euismod semper. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla.</p>\r\n<p><a class=\"btn btn-primary mt-4\" href=\"/services\">View Services</a></p>',1,0,1,2,'2018-02-09 13:07:53','2018-02-16 12:42:42'),
	(13,NULL,'News','news','<div class=\"text-center\">\r\n\r\n# News\r\n\r\nAll the latest news from Grace Tarneit as well as exclusive land releases & information about the community. \r\n\r\n{# blog-feed(\'http://example.com/feed.json\') #}','<p><div class=\"text-center\"></p>\r\n<h1 id=\"news\">News</h1>\r\n<p>All the latest news from Grace Tarneit as well as exclusive land releases &amp; information about the community. </p>\r\n<p>{# blog-feed(\'http://example.com/feed.json\') #}</p>',1,0,1,3,'2018-02-14 14:07:04','2018-02-16 12:45:19'),
	(14,NULL,'Contact','contact','# Contact Us\r\n\r\nMaecenas sed diam eget risus varius blandit sit. Duis mollis, est non commodo luctus, nisi erat. Diam eget risus varius blandit sit. Duis mollis, est non commodo luctus, nisi erat.\r\n\r\n{# contact-form(\'mX\') #}','<h1 id=\"contactus\">Contact Us</h1>\r\n<p>Maecenas sed diam eget risus varius blandit sit. Duis mollis, est non commodo luctus, nisi erat. Diam eget risus varius blandit sit. Duis mollis, est non commodo luctus, nisi erat.</p>\r\n<p>{# contact-form(\'mX\') #}</p>',1,0,1,5,'2018-02-14 15:14:05','2018-02-15 17:11:24'),
	(15,NULL,'Services','services','{# services-intro() #}\r\n\r\n---\r\n\r\n{# categories-list() #}','<p>{# services-intro() #}</p>\r\n<hr />\r\n<p>{# categories-list() #}</p>',1,1,1,1,'2018-02-15 14:40:03','2018-02-16 15:57:43'),
	(16,15,'Service','service','{# service-details-intro() #}\r\n\r\n---\r\n\r\n{# category-items() #}','<p>{# service-details-intro() #}</p>\r\n<hr />\r\n<p>{# category-items() #}</p>',1,0,0,1,'2018-02-15 16:00:21','2018-02-16 10:24:55'),
	(17,NULL,'Terms and Conditions','terms-and-conditions','# Tarneit Projects Private Concierge Services Terms and Conditions (Terms)\r\n\r\nThis Website is owned and operated by Tarneit Projects Pty Ltd (ABN 16 607 144 258) (Tarneit Projects).\r\nIn these Terms, “we” and “us” mean Tarneit Projects and “you” means you, the Resident.\r\nA \"Resident\" is a registered user or customer of the Website who submits an inquiry or request for assistance to the Concierge Platform. \r\nA \"Service Provider\" is an independent third party providing goods and services relevant to the Concierge Platform. \r\nThe \"Estate\" is the Grace Tarneit Estate managed by Tarneit Projects.\r\nThe service offered by Tarneit Projects is an on demand concierge services platform for Residents to request information regarding the Estate and local community and to request service information about available Service Providers.\r\nIt is a condition of use of this Website that you acknowledge and agree that Tarneit Projects is merely providing an information based service for eligible Residents. Any transactions involving the provision and receipt of services that arise from any dealings between Residents and Service Providers are the sole responsibility of the Resident and Service Provider. \r\nAccess to and use of the Website and the services available through the Website are subject to the following Terms which may be updated by us from time to time.\r\n1.	Your acceptance\r\n(a)	These are the terms on which Tarneit Projects permits Residents to access and use the Tarneit Projects website [TO BE CONFIRMED] (Website) including using the services and functionality made available through the Website, viewing Content provided by Tarneit Projects and third parties, communicating with Tarneit Projects, reviewing service information and submitting information requests and/or requests for services (Requests) through the online concierge platform (Concierge Platform).\r\n(b)	You agree to be bound by these Terms by:\r\n	(i)	using, browsing or accessing any part of the Website; \r\n	(ii)	registering as a Resident on the Website; or\r\n	(iii)	submitting a Request through the Concierge Platform.\r\n(c)	If you do not agree to these Terms, you are not permitted to access and use this Website and you should immediately cease using this Website.\r\n(d)	Tarneit Projects may from time to time review and update these Terms to take account of new laws, regulations, products or technology.  Your use of the Website will be governed by the most recent Terms posted on the Website.  By continuing to use the Website, you agree to be bound by the most recent Terms.  It is your responsibility to check the Website regularly for updated versions of the Terms.\r\n(e)	The Website is subject to change at any time without notice and may contain errors. \r\n(f)	Tarneit Projects reserves the right to withdraw the Services from the Website at any time at their discretion.\r\n2.	Tarneit Projects Services  \r\n(a)	We provide a freely available service on this Website which enables Residents to submit Requests on the Website regarding the Estate, local community and broader Tarneit area. The services is intended to make the transition into your new home easier and to let you know more about the community you are living in. Our qualified Concierge Platform staff will respond to Requests within 48 hours Monday to Friday from 9am to 5pm.\r\n(b)	The Concierge Platform will be available from 1st quarter 2018 to December 31st 2020.  We will advise you of exact dates closer to opening time and are happy to register you in advance.  Any changes to the commencement date will be posted on the Website. \r\n3.	Resident registration and passwords\r\n(a)	The Concierge Platform is available only to original owner occupier purchasers of allotments forming part of the Estate. It is not available to related parties of the original owner occupier purchaser or residents who subsequently buy into the Estate or lease properties on the Estate. In order to access the Concierge Platform you will be required to register your details and log-in with a unique user name and password (Password).  We will assess your eligibility and once approved will use your registration information to keep in contact with you and enable access to the Concierge Platform. \r\n(b)	You warrant that all information and data provided by you in the registration is accurate, complete and up to date. You will promptly inform us if there is any change to this information or data.\r\n(c)	You may only issue a Request on the Concierge Platform by entering the account user name and Password created during the registration process.  You agree that you will not disclose, or permit disclosure of, the Password to any person. You will be fully responsible for all acts and omissions of any person who enters into a transaction using your Password, as if they were your own acts and omissions. We will not in any event be liable for any loss, damage, claims, costs or expenses arising out of the use or misuse of the Password, and you will indemnify us against all loss, damage, claims, costs or demands in this regard.\r\n(d)	You may elect to change the Password at any time using the facility provided on the Website.  You must immediately notify us of any Password which is lost, inoperable or used in an unauthorised manner.\r\n4.	Issuing Requests through the Concierge Platform\r\n(a)	Once a Resident submits a Request, we will respond to your email within 48 hours.  Tarneit Projects reserves the right to withdraw, with or without prior notice, any Request that may in Tarneit Projects\' view be in breach of these Terms, or any law or regulation, or any published Tarneit Projects policy. \r\n(b)	Where you request information about a Service Provider we will provide their contact details so you can contact the Service Provider directly. Please note Residents are not bound to appoint any Service Provider at any stage.  Should you wish to appoint a Service Provider any subsequent transaction and / or arrangements are independent of Tarneit Projects and the parties agree to deal directly with one another using the contact details provided to finalise any service transactions. \r\n(c)	You acknowledge and agree that you are solely responsible for the information contained in a Request and that all information and data provided by you is accurate, complete and up to date. \r\n5.	Service information and availability\r\n(a)	The listing of any particular Service Provider and information on our Website is for information and research purposes only.  Tarneit Projects does not warrant or make any representations in relation to any information or Service Providers listed. \r\n(b)	We are not responsible or liable for any loss or damage you or any third party may suffer or incur in connection with any service you obtain by using this Website or for any acts, omissions, errors or defaults of any third party in connection with a service.\r\n(c)	Whilst every effort is made to ensure the information provided on the Website is current, we have no responsibility or liability for any errors contained in the information. Inclusion of any service information on this Website is not an endorsement of any organisation or service.\r\n(d)	Tarneit Projects has the right, but not the obligation, to monitor any information, comment, content, communication, advice, text, or other material (Content) made available on the Website.  Tarneit Projects reserves the right, in its absolute discretion, to block, modify or remove any Content contained on the Website without notice, and will not be liable in any way for possible consequences of such actions.  \r\n6.	Intellectual property rights\r\n(a)	All intellectual property rights, including copyright and patents, in the Website, Tarneit Projects\' goods and services, and all components of them are owned or licensed by Tarneit Projects or any of its related entities.  You must not copy, modify or transmit any part of the Website.\r\n(b)	The Website contains trademarks, logos, service names and trade names of Tarneit Projects or third parties which may be registered or otherwise protected by law.  You are not permitted to use any trademarks, logos, service names, trade names or any other content or copies of the content appearing on the Website.\r\n7.	Links\r\n(a)	The Website may contain links to other web sites.  Tarneit Projects provides those links as a ready reference for searching for third party services on the Internet and not as an endorsement of those web sites, their operators, the services or content that they describe.\r\n(b)	Other web sites which are linked to the Website are not covered by these Terms, and may have their own terms and conditions and privacy policy.  If you choose to access these linked sites, you do so at your own risk.  Tarneit Projects is not responsible for and will not be liable in respect of the content or operation of those web sites or any of the goods, services or content that they describe. Tarneit Projects is not responsible for and will not be liable in respect of any incorrect link to an external web site.\r\n(c)	You are not permitted to frame or link the Website without Tarneit Projects\' express written permission.\r\n8.	Access and communication\r\n(a)	Subject to the consumer guarantees provided for in consumer protection legislation (including the Australian Consumer Law), Tarneit Projects does not warrant that you will have continuous access to the Website. Tarneit Projects will not be liable in the event that the Website is unavailable to you due to computer downtime attributable to malfunctions, upgrades, preventative or remedial maintenance activities or interruption in telecommunications supply.\r\n(b)	Tarneit Projects does not guarantee the delivery of communications over the Internet as such communications rely on third party service providers.  Electronic communication (including electronic mail) is vulnerable to interception by third parties and Tarneit Projects does not guarantee the security or confidentiality of these communications or the security of the Website.\r\n(c)	Tarneit Projects does not provide, and has no control over, communications, networks or services, the Internet or other technology required or used across the Website and accepts no responsibility for any direct or indirect loss in any form associated with them, whether due to congestion, technical malfunction, viruses or otherwise. \r\n(d)	Details contained on the Website relating to goods and services have been prepared in accordance with Australian law and may not satisfy the laws of another country.  Tarneit Projects does not warrant that: \r\n	(i)	the goods or services available on this Website; or\r\n	(ii)	the Website and its Content,  \r\ncomply with the laws of any other country.  It is your responsibility to determine whether the goods or services comply with the laws of your jurisdiction.\r\n(e)	If you access and use this Website or its Content from outside Australia or otherwise transact on this Website you do so at your own risk.\r\n9.	Privacy\r\nAny personal information submitted by you to Tarneit Projects is subject to and will be handled in accordance with Tarneit Projects\' privacy policy (Privacy Policy).  The Privacy Policy forms part of these Terms and is set out at [to be confirmed] You agree that, by using the Website or communicating with Tarneit Projects, you have read the Privacy Policy, understood its contents and consented to its requirements.\r\n10.	Website licence and use \r\nTarneit Projects grants you a non-exclusive and non-transferable licence to use the Website for your own personal use subject to the use requirements specified in this clause.  You may not download (other than page caching) or modify the Website or any portion of the Website.  Any Content that you post on the Website or otherwise provide or communicate to Tarneit Projects will be treated as non-confidential and non-proprietary information.\r\n11.	Prohibited uses\r\n(a)	Except as otherwise permitted in submitting Requests using the Concierge Platform, you must not:\r\n	(i)	engage in any separate commercial activity including marketing, advertising or commercial promotion of goods or services, resale, collect and use any product lists or information for the benefit of other merchants, data mine or use robots or other data collection methods;\r\n	(ii)	impersonate or falsely claim to represent a person or organisation;\r\n	(iii)	defame, abuse, stalk, harass, threaten or otherwise violate the legal rights of others, including without limitation, rights relating to privacy and publicity;\r\n	(iv)	post, link to, or otherwise communicate or distribute any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful material or information, or otherwise use the Website in a manner which is unlawful or would infringe the rights of another person including any intellectual property rights; or\r\n	(v)	post, link to, or otherwise distribute any information, material or item which contains a virus, trojan horse, worm or other harmful or disruptive component.\r\n(b)	Unauthorised use of the Website may give rise to a claim for damages and/or may result in legal proceedings being taken against you.\r\n(c)	Tarneit Projects provides no warranties and cannot guarantee that any file, program, access or use of the Website is free from viruses, malware or other harmful technology or material which could damage or infect your data, hardware, software or other equipment. By accessing and using the Website (including submitting Requests through the Concierge Platform) you assume all risk in this regard and you release Tarneit Projects from all applicable liability and responsibility.\r\n12.	Termination of your access to the Website\r\nTarneit Projects may at any time immediately terminate the Services or your access (including restricting access) to the Website or any feature of the Website for any reason (including due to your breach or alleged breach of these Terms) in its sole discretion and without prior notice.  Any indemnities given by you and any limitations of our liability survive such termination.\r\n13.	Disclaimer of warranties and limitation of liability \r\n(a)	To the full extent permitted by law, Tarneit Projects excludes all warranties, whether express or implied, including any warranties or representations concerning availability of the Website, quality, completeness, accuracy, suitability, acceptability or fitness for purpose in relation to the Website, the Content, the conduct of any users, all links to or from the Website and the goods and services listed or accessible on the Website. \r\n(b)	Subject to the consumer guarantees provided for in consumer protection legislation (including the Australian Consumer Law) Tarneit Projects excludes all liability for any loss, damage, claim, cost or expense whatsoever arising out of or in connection with these Terms, the Website, the Content, all links to or from the Website.\r\n(c)	Subject to the consumer guarantees provided for in consumer protection legislation (including the Australian Consumer Law) Tarneit Projects excludes all liability for any loss, damage, claim, cost or expense whatsoever arising out of or in connection with the goods and services listed, accessible or made available on the Website. \r\n14.	Indemnity\r\nYou agree to fully indemnify Tarneit Projects, its directors, officers, directors, employees, consultants, agents and affiliates in respect of all loss, damage, costs, expenses (including legal fees on a full indemnity basis), fines, penalties, claims, demands and proceedings howsoever arising, whether at common law (including negligence) or under statute, in connection with any of the following:\r\n(a)	any breach of these Terms by you;\r\n(b)	your access or use of the Website;\r\n(c)	any Request on the Website; or\r\n(d)	your communications with Tarneit Projects, Residents, Service Providers or any other third party.\r\n15.	Jurisdiction and law\r\nThese Terms are governed by and must be construed in accordance with the laws of the State of Victoria, Australia.  You submit to the exclusive jurisdiction of the courts of that State and the Commonwealth of Australia in respect of all matters arising out of or relating to these Terms, their performance and subject matter.\r\n16.	Waiver\r\nIf you breach these conditions and we take no action, we will still be entitled to use our rights and remedies in any other situation where you breach these Terms.\r\n17.	Severability\r\nEach provision of these Terms is severable from the others and no severance of a provision will affect any other provision.\r\n18.	Entire Agreement \r\nThe above Terms constitute the entire agreement of the parties and supersede any and all preceding and contemporaneous agreements between you and Tarneit Projects. Any waiver of any provision of the Terms will be effective only if in writing and signed by a Director of Tarneit Projects.\r\n19.	Contacting us\r\nIf you have questions about the Website, the Terms or Privacy Policy, please contact us at [to be confirmed]\r\nLast updated: April 11, 2017\r\n','<h1 id=\"tarneitprojectsprivateconciergeservicestermsandconditionsterms\">Tarneit Projects Private Concierge Services Terms and Conditions (Terms)</h1>\r\n<p>This Website is owned and operated by Tarneit Projects Pty Ltd (ABN 16 607 144 258) (Tarneit Projects).<br />\r\nIn these Terms, “we” and “us” mean Tarneit Projects and “you” means you, the Resident.<br />\r\nA \"Resident\" is a registered user or customer of the Website who submits an inquiry or request for assistance to the Concierge Platform. <br />\r\nA \"Service Provider\" is an independent third party providing goods and services relevant to the Concierge Platform. <br />\r\nThe \"Estate\" is the Grace Tarneit Estate managed by Tarneit Projects.<br />\r\nThe service offered by Tarneit Projects is an on demand concierge services platform for Residents to request information regarding the Estate and local community and to request service information about available Service Providers.<br />\r\nIt is a condition of use of this Website that you acknowledge and agree that Tarneit Projects is merely providing an information based service for eligible Residents. Any transactions involving the provision and receipt of services that arise from any dealings between Residents and Service Providers are the sole responsibility of the Resident and Service Provider. <br />\r\nAccess to and use of the Website and the services available through the Website are subject to the following Terms which may be updated by us from time to time.</p>\r\n<ol>\r\n<li>Your acceptance<br />\r\n(a)    These are the terms on which Tarneit Projects permits Residents to access and use the Tarneit Projects website <a href=\"Website\">TO BE CONFIRMED</a> including using the services and functionality made available through the Website, viewing Content provided by Tarneit Projects and third parties, communicating with Tarneit Projects, reviewing service information and submitting information requests and/or requests for services (Requests) through the online concierge platform (Concierge Platform).<br />\r\n(b)    You agree to be bound by these Terms by:<br />\r\n(i) using, browsing or accessing any part of the Website; <br />\r\n(ii)    registering as a Resident on the Website; or<br />\r\n(iii)   submitting a Request through the Concierge Platform.<br />\r\n(c)    If you do not agree to these Terms, you are not permitted to access and use this Website and you should immediately cease using this Website.<br />\r\n(d)    Tarneit Projects may from time to time review and update these Terms to take account of new laws, regulations, products or technology.  Your use of the Website will be governed by the most recent Terms posted on the Website.  By continuing to use the Website, you agree to be bound by the most recent Terms.  It is your responsibility to check the Website regularly for updated versions of the Terms.<br />\r\n(e)    The Website is subject to change at any time without notice and may contain errors. <br />\r\n(f)    Tarneit Projects reserves the right to withdraw the Services from the Website at any time at their discretion.</li>\r\n<li>Tarneit Projects Services  <br />\r\n(a)    We provide a freely available service on this Website which enables Residents to submit Requests on the Website regarding the Estate, local community and broader Tarneit area. The services is intended to make the transition into your new home easier and to let you know more about the community you are living in. Our qualified Concierge Platform staff will respond to Requests within 48 hours Monday to Friday from 9am to 5pm.<br />\r\n(b)    The Concierge Platform will be available from 1st quarter 2018 to December 31st 2020.  We will advise you of exact dates closer to opening time and are happy to register you in advance.  Any changes to the commencement date will be posted on the Website. </li>\r\n<li>Resident registration and passwords<br />\r\n(a)    The Concierge Platform is available only to original owner occupier purchasers of allotments forming part of the Estate. It is not available to related parties of the original owner occupier purchaser or residents who subsequently buy into the Estate or lease properties on the Estate. In order to access the Concierge Platform you will be required to register your details and log-in with a unique user name and password (Password).  We will assess your eligibility and once approved will use your registration information to keep in contact with you and enable access to the Concierge Platform. <br />\r\n(b)    You warrant that all information and data provided by you in the registration is accurate, complete and up to date. You will promptly inform us if there is any change to this information or data.<br />\r\n(c)    You may only issue a Request on the Concierge Platform by entering the account user name and Password created during the registration process.  You agree that you will not disclose, or permit disclosure of, the Password to any person. You will be fully responsible for all acts and omissions of any person who enters into a transaction using your Password, as if they were your own acts and omissions. We will not in any event be liable for any loss, damage, claims, costs or expenses arising out of the use or misuse of the Password, and you will indemnify us against all loss, damage, claims, costs or demands in this regard.<br />\r\n(d)    You may elect to change the Password at any time using the facility provided on the Website.  You must immediately notify us of any Password which is lost, inoperable or used in an unauthorised manner.</li>\r\n<li>Issuing Requests through the Concierge Platform<br />\r\n(a)    Once a Resident submits a Request, we will respond to your email within 48 hours.  Tarneit Projects reserves the right to withdraw, with or without prior notice, any Request that may in Tarneit Projects\' view be in breach of these Terms, or any law or regulation, or any published Tarneit Projects policy. <br />\r\n(b)    Where you request information about a Service Provider we will provide their contact details so you can contact the Service Provider directly. Please note Residents are not bound to appoint any Service Provider at any stage.  Should you wish to appoint a Service Provider any subsequent transaction and / or arrangements are independent of Tarneit Projects and the parties agree to deal directly with one another using the contact details provided to finalise any service transactions. <br />\r\n(c)    You acknowledge and agree that you are solely responsible for the information contained in a Request and that all information and data provided by you is accurate, complete and up to date. </li>\r\n<li>Service information and availability<br />\r\n(a)    The listing of any particular Service Provider and information on our Website is for information and research purposes only.  Tarneit Projects does not warrant or make any representations in relation to any information or Service Providers listed. <br />\r\n(b)    We are not responsible or liable for any loss or damage you or any third party may suffer or incur in connection with any service you obtain by using this Website or for any acts, omissions, errors or defaults of any third party in connection with a service.<br />\r\n(c)    Whilst every effort is made to ensure the information provided on the Website is current, we have no responsibility or liability for any errors contained in the information. Inclusion of any service information on this Website is not an endorsement of any organisation or service.<br />\r\n(d)    Tarneit Projects has the right, but not the obligation, to monitor any information, comment, content, communication, advice, text, or other material (Content) made available on the Website.  Tarneit Projects reserves the right, in its absolute discretion, to block, modify or remove any Content contained on the Website without notice, and will not be liable in any way for possible consequences of such actions.  </li>\r\n<li>Intellectual property rights<br />\r\n(a)    All intellectual property rights, including copyright and patents, in the Website, Tarneit Projects\' goods and services, and all components of them are owned or licensed by Tarneit Projects or any of its related entities.  You must not copy, modify or transmit any part of the Website.<br />\r\n(b)    The Website contains trademarks, logos, service names and trade names of Tarneit Projects or third parties which may be registered or otherwise protected by law.  You are not permitted to use any trademarks, logos, service names, trade names or any other content or copies of the content appearing on the Website.</li>\r\n<li>Links<br />\r\n(a)    The Website may contain links to other web sites.  Tarneit Projects provides those links as a ready reference for searching for third party services on the Internet and not as an endorsement of those web sites, their operators, the services or content that they describe.<br />\r\n(b)    Other web sites which are linked to the Website are not covered by these Terms, and may have their own terms and conditions and privacy policy.  If you choose to access these linked sites, you do so at your own risk.  Tarneit Projects is not responsible for and will not be liable in respect of the content or operation of those web sites or any of the goods, services or content that they describe. Tarneit Projects is not responsible for and will not be liable in respect of any incorrect link to an external web site.<br />\r\n(c)    You are not permitted to frame or link the Website without Tarneit Projects\' express written permission.</li>\r\n<li>Access and communication<br />\r\n(a)    Subject to the consumer guarantees provided for in consumer protection legislation (including the Australian Consumer Law), Tarneit Projects does not warrant that you will have continuous access to the Website. Tarneit Projects will not be liable in the event that the Website is unavailable to you due to computer downtime attributable to malfunctions, upgrades, preventative or remedial maintenance activities or interruption in telecommunications supply.<br />\r\n(b)    Tarneit Projects does not guarantee the delivery of communications over the Internet as such communications rely on third party service providers.  Electronic communication (including electronic mail) is vulnerable to interception by third parties and Tarneit Projects does not guarantee the security or confidentiality of these communications or the security of the Website.<br />\r\n(c)    Tarneit Projects does not provide, and has no control over, communications, networks or services, the Internet or other technology required or used across the Website and accepts no responsibility for any direct or indirect loss in any form associated with them, whether due to congestion, technical malfunction, viruses or otherwise. <br />\r\n(d)    Details contained on the Website relating to goods and services have been prepared in accordance with Australian law and may not satisfy the laws of another country.  Tarneit Projects does not warrant that: <br />\r\n(i) the goods or services available on this Website; or<br />\r\n(ii)    the Website and its Content,  <br />\r\ncomply with the laws of any other country.  It is your responsibility to determine whether the goods or services comply with the laws of your jurisdiction.<br />\r\n(e)    If you access and use this Website or its Content from outside Australia or otherwise transact on this Website you do so at your own risk.</li>\r\n<li>Privacy<br />\r\nAny personal information submitted by you to Tarneit Projects is subject to and will be handled in accordance with Tarneit Projects\' privacy policy (Privacy Policy).  The Privacy Policy forms part of these Terms and is set out at [to be confirmed] You agree that, by using the Website or communicating with Tarneit Projects, you have read the Privacy Policy, understood its contents and consented to its requirements.</li>\r\n<li>Website licence and use <br />\r\nTarneit Projects grants you a non-exclusive and non-transferable licence to use the Website for your own personal use subject to the use requirements specified in this clause.  You may not download (other than page caching) or modify the Website or any portion of the Website.  Any Content that you post on the Website or otherwise provide or communicate to Tarneit Projects will be treated as non-confidential and non-proprietary information.</li>\r\n<li>Prohibited uses<br />\r\n(a)    Except as otherwise permitted in submitting Requests using the Concierge Platform, you must not:<br />\r\n(i) engage in any separate commercial activity including marketing, advertising or commercial promotion of goods or services, resale, collect and use any product lists or information for the benefit of other merchants, data mine or use robots or other data collection methods;<br />\r\n(ii)    impersonate or falsely claim to represent a person or organisation;<br />\r\n(iii)   defame, abuse, stalk, harass, threaten or otherwise violate the legal rights of others, including without limitation, rights relating to privacy and publicity;<br />\r\n(iv)    post, link to, or otherwise communicate or distribute any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful material or information, or otherwise use the Website in a manner which is unlawful or would infringe the rights of another person including any intellectual property rights; or<br />\r\n(v) post, link to, or otherwise distribute any information, material or item which contains a virus, trojan horse, worm or other harmful or disruptive component.<br />\r\n(b)    Unauthorised use of the Website may give rise to a claim for damages and/or may result in legal proceedings being taken against you.<br />\r\n(c)    Tarneit Projects provides no warranties and cannot guarantee that any file, program, access or use of the Website is free from viruses, malware or other harmful technology or material which could damage or infect your data, hardware, software or other equipment. By accessing and using the Website (including submitting Requests through the Concierge Platform) you assume all risk in this regard and you release Tarneit Projects from all applicable liability and responsibility.</li>\r\n<li>Termination of your access to the Website<br />\r\nTarneit Projects may at any time immediately terminate the Services or your access (including restricting access) to the Website or any feature of the Website for any reason (including due to your breach or alleged breach of these Terms) in its sole discretion and without prior notice.  Any indemnities given by you and any limitations of our liability survive such termination.</li>\r\n<li>Disclaimer of warranties and limitation of liability <br />\r\n(a)    To the full extent permitted by law, Tarneit Projects excludes all warranties, whether express or implied, including any warranties or representations concerning availability of the Website, quality, completeness, accuracy, suitability, acceptability or fitness for purpose in relation to the Website, the Content, the conduct of any users, all links to or from the Website and the goods and services listed or accessible on the Website. <br />\r\n(b)    Subject to the consumer guarantees provided for in consumer protection legislation (including the Australian Consumer Law) Tarneit Projects excludes all liability for any loss, damage, claim, cost or expense whatsoever arising out of or in connection with these Terms, the Website, the Content, all links to or from the Website.<br />\r\n(c)    Subject to the consumer guarantees provided for in consumer protection legislation (including the Australian Consumer Law) Tarneit Projects excludes all liability for any loss, damage, claim, cost or expense whatsoever arising out of or in connection with the goods and services listed, accessible or made available on the Website. </li>\r\n<li>Indemnity<br />\r\nYou agree to fully indemnify Tarneit Projects, its directors, officers, directors, employees, consultants, agents and affiliates in respect of all loss, damage, costs, expenses (including legal fees on a full indemnity basis), fines, penalties, claims, demands and proceedings howsoever arising, whether at common law (including negligence) or under statute, in connection with any of the following:<br />\r\n(a)    any breach of these Terms by you;<br />\r\n(b)    your access or use of the Website;<br />\r\n(c)    any Request on the Website; or<br />\r\n(d)    your communications with Tarneit Projects, Residents, Service Providers or any other third party.</li>\r\n<li>Jurisdiction and law<br />\r\nThese Terms are governed by and must be construed in accordance with the laws of the State of Victoria, Australia.  You submit to the exclusive jurisdiction of the courts of that State and the Commonwealth of Australia in respect of all matters arising out of or relating to these Terms, their performance and subject matter.</li>\r\n<li>Waiver<br />\r\nIf you breach these conditions and we take no action, we will still be entitled to use our rights and remedies in any other situation where you breach these Terms.</li>\r\n<li>Severability<br />\r\nEach provision of these Terms is severable from the others and no severance of a provision will affect any other provision.</li>\r\n<li>Entire Agreement <br />\r\nThe above Terms constitute the entire agreement of the parties and supersede any and all preceding and contemporaneous agreements between you and Tarneit Projects. Any waiver of any provision of the Terms will be effective only if in writing and signed by a Director of Tarneit Projects.</li>\r\n<li>Contacting us<br />\r\nIf you have questions about the Website, the Terms or Privacy Policy, please contact us at [to be confirmed]<br />\r\nLast updated: April 11, 2017</li>\r\n</ol>',1,0,0,NULL,'2018-02-16 11:05:47','2018-02-16 12:15:12');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `key`, `value`)
VALUES
	(1,'google-analytics-key',''),
	(2,'mailchimp-api-key','c2be8ff2c4e8428e8b9f1bff4c73f250-us17'),
	(3,'activecampaign-api-key',''),
	(4,'logo-path','http://codeigniter.loc:8888/assets/uploads/Jakesalad.png');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`)
VALUES
	(1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com',NULL,NULL,NULL,'u51HweC46Dyoww7kYdEs2e',1268889823,1518756357,1,'Admin','istrator','ADMIN','0'),
	(2,'::1','member@member.com','$2y$08$4uZDDnN2orFoAVo3Cubpd.dMI.j/on6flJNnFytD.YO5YO1FrUf2u',NULL,'member@member.com',NULL,NULL,NULL,NULL,1517547996,NULL,1,'Mem','ber','','');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,2,2);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
