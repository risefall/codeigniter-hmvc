<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Filemanager {
  
  var $ci;
   
  function __construct() {
      $this->ci =& get_instance();

      $config = [
        'security' => [
          'readOnly' => true,
          'extensions' => [
            'policy' => 'ALLOW_LIST',
            'restrictions' => [
              'jpg',
              'jpe',
              'jpeg',
              'gif',
              'png',
              'html',
            ],
          ],
        ],
        "upload" => [
          "fileSizeLimit" => 2000000, // 2MB
          "overwrite" => false,
        ],
        "images" => [
          "main" => [
            "autoOrient" => true,
            "maxWidth" => 1280,
            "maxHeight" => 1024,
          ],
          "thumbnail" => [
            "enabled" => true,
            "cache" => true,
            "dir" => "_thumbs/",
            "crop" => true,
            "maxHeight" => 64,
          ]
        ],
        "mkdir_mode" => 0755,
      ];

      $app = new \RFM\Application();

      // local filesystem storage
      $local = new \RFM\Repository\Local\Storage($config);
      $app->setStorage($local);
      $local->setRoot(FCPATH.'assets/uploads', true, false);

      // local filesystem API
      $app->api = new RFM\Api\LocalApi();
  }

}