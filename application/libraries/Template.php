<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Template 
{
    var $ci;
     
    function __construct() 
    {
        $this->ci =& get_instance();
    }

    function load($tpl_view, $body_view = null, $data = null) 
    {
        if ( ! is_null( $body_view ) ) 
        {
            if ( file_exists( APPPATH.'modules/'.$tpl_view.'/views/'.$body_view ) ) 
            {
                $body_view_path = $tpl_view.'/'.$body_view;
            }
            else if ( file_exists( APPPATH.'modules/'.$tpl_view.'/views/'.$body_view.'.twig' ) ) 
            {
                $body_view_path = $tpl_view.'/'.$body_view.'.twig';
            }
            else if ( file_exists( APPPATH.'views/'.$tpl_view.'/'.$body_view ) ) 
            {
                $body_view_path = $tpl_view.'/'.$body_view;
            }
            else if ( file_exists( APPPATH.'views/'.$tpl_view.'/'.$body_view.'.twig' ) ) 
            {
                $body_view_path = $tpl_view.'/'.$body_view.'.twig';
            }
            else if ( file_exists( APPPATH.'views/'.$body_view ) ) 
            {
                $body_view_path = $body_view;
            }
            else if ( file_exists( APPPATH.'views/'.$body_view.'.twig' ) ) 
            {
                $body_view_path = $body_view.'.twig';
            }
            else
            {
                show_error('Unable to load the requested file: ' . $tpl_view.'/'.$body_view.'.twig');
            }
             
            $body = $this->ci->load->view($body_view_path, $data, TRUE);
             
            if ( is_null($data) ) 
            {
                $data = array('body' => $body);
            }
            else if ( is_array($data) )
            {
                $data['body'] = $body;
            }
            else if ( is_object($data) )
            {
                $data->body = $body;
            }
        }

        $loader = new Twig_Loader_Filesystem(APPPATH.'views/templates');
        // $loader = new Twig_Loader_Filesystem(APPPATH.'modules/page/views');
        // $loader = new Twig_Loader_Filesystem(APPPATH.'modules/page/views');

        $twig = new Twig_Environment($loader, array(
            'debug' => true,
            // 'cache' => APPPATH.'cache',
        ));
        $twig->addExtension(new Twig_Extension_StringLoader());
        $twig->addExtension(new Twig_Extension_Debug());

        // $twig = new Twig_Environment($loader, array(
        //     'cache' => APPPATH.'cache',
        // ));

        // $template = $twig->load('index.twig');

        $this->ci->load->model('auth/settings_model');
        $this->ci->load->library('ion_auth');

        $data['settings'] = $this->ci->settings_model->get_array();
        $data['user'] = $this->ci->ion_auth->user()->row();

        echo $twig->render($tpl_view.'.twig', $data);
         
        // $this->ci->load->view('templates/'.$tpl_view, $data);
    }


}