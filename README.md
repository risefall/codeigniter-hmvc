# CodeIgniter3 CMS

## Setup & Installation

### Build

`composer install`

https://getcomposer.org

`npm install`

https://www.npmjs.com/

`bower install`

https://bower.io/

`gulp`

https://gulpjs.com/

### Configure

#### App/Database

Rename and configure `config/example.config.php` and `config/example.database.php`

#### Rich Filemanager

Copy `/assets/json/filemanager.config.json` into `/bower_components/rich-filemanager/config`

## Built with

- PHP 7.0
- Codeigniter - https://github.com/bcit-ci/CodeIgniter
- Ion Auth 2 - https://github.com/benedmunds/CodeIgniter-Ion-Auth
- Rich File Manager - https://github.com/servocoder/RichFilemanager
- Symfony Yaml - https://symfony.com/doc/current/components/yaml/index.html
- Composer
- Bower
- NPM
- Gulp

## Roadmap

Figure out roadmap for development and update it here